import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Form01 extends JFrame {

    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JButton calculateButton;
    private JCheckBox glassTypeCheckBox;
    private JPanel panel1;
    private JButton clickToUploadAButton;
    private JSpinner spinner2;
    private JSpinner spinner1;
    private JSpinner spinner3;
    private JSpinner spinner4;
    private JSpinner spinner5;
    private JSpinner spinner6;
    private JSpinner spinner7;
    private JSpinner spinner8;
    private JButton sortFileButton;
    private JComboBox comboBox1;
    private JComboBox comboBox2;
    private JComboBox comboBox3;
    private JComboBox comboBox4;

    public  Form01(){
      add(panel1);
      setSize(800,800);
      setLocation(600,600);
        setTitle("CSVEditor");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        calculateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
        String spot, x, y, z;
        int a,b,c,d, resultFirst, resultSecond;
            spot = textField2.getText();
                x = textField3.getText();
                y = textField4.getText();
                z = textField5.getText();
               a=Integer.parseInt(spot);
               b=  Integer.parseInt(x);
               c=  Integer.parseInt(y);
                d= Integer.parseInt(z);
                 resultFirst = 4 + (2 * (a / b));

             if(glassTypeCheckBox.isSelected()){
                 resultSecond=  (2 * (c * (a / (b * d)))+2); }

                 else{
                    resultSecond = 2 * (c * (a / (b * d)));
                }


              //  System.out.println("Note: If you are using a glass type 2nd reference sample, you should make a plus 2.");
                JOptionPane optionPane= new JOptionPane(
                        "You need to use '" + resultFirst + "' pcs 1. ref sample\n"
                                +"You need to use '" + resultSecond + "' pcs 2. ref sample\n",
                        JOptionPane.PLAIN_MESSAGE
                );
                JDialog dialog = optionPane.createDialog("Reference Numbers!");
                dialog.setAlwaysOnTop(true);
                dialog.setVisible(true);

            }
        });
        clickToUploadAButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
                dialog.setMode(FileDialog.LOAD);
                dialog.setVisible(true);
                String file = dialog.getFile();
                System.out.println(file + " chosen.");
            }
        });

        sortFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //ref1
               int a =(Integer) spinner1.getValue();
                //to
             int b =  (Integer) spinner2.getValue();
                //ref2
                int c =  (Integer) spinner3.getValue();
                //to
                int d=  (Integer) spinner4.getValue();
                int f =  (Integer)  spinner5.getValue();
                //to
                int g=  (Integer)  spinner6.getValue();
                int h =  (Integer)    spinner7.getValue();
                //to
                int i =  (Integer)   spinner8.getValue();

                String  aho= (String) comboBox1.getSelectedItem();
                System.out.println(aho);
            }
        });
    }



}
