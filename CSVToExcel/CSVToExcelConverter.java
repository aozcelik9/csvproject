package CSVToExcel;

import java.io.*;
import java.util.*;

import Swing.Swing1;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CSVToExcelConverter {

    public static void csvToExcel(String file) throws IOException
    {
        ArrayList arList=null;
        ArrayList al=null;
        String fName = file;
        String thisLine;
        int count=0;
        FileInputStream fis = new FileInputStream(fName);
        DataInputStream myInput = new DataInputStream(fis);
        int i=0;
        arList = new ArrayList();
        while ((thisLine = myInput.readLine()) != null)
        {
            al = new ArrayList();
            String strar[] = thisLine.split(",");
            for(int j=0;j<strar.length;j++)
            {
                al.add(strar[j]);
            }
            arList.add(al);
            System.out.println();
            i++;
        }

        try
        {
            HSSFWorkbook hwb = new HSSFWorkbook();
            HSSFSheet sheet = hwb.createSheet("new sheet");
            for(int k=0;k<arList.size();k++)
            {
                ArrayList ardata = (ArrayList)arList.get(k);
                HSSFRow row = sheet.createRow((short) 0+k);
                for(int p=0;p<ardata.size();p++)
                {
                    HSSFCell cell = row.createCell((short) p);
                    String data = ardata.get(p).toString();
                    if(data.startsWith("=")){
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        data=data.replaceAll("\"", "");
                        data=data.replaceAll("=", "");
                        cell.setCellValue(data);
                    }else if(data.startsWith("\"")){
                        data=data.replaceAll("\"", "");
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(data);
                    }else{
                        data=data.replaceAll("\"", "");
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(data);
                    }

                }
                System.out.println();
            }
            FileOutputStream fileOut = new FileOutputStream("test.xls");
            hwb.write(fileOut);
            fileOut.close();
            System.out.println("Your excel file has been generated");
        } catch ( Exception ex ) {
            ex.printStackTrace();
        } //main method ends
    }

    public static void readExcel(String file) throws IOException {
        InputStream ExcelFileToRead = new FileInputStream(file);
        Workbook wb = new HSSFWorkbook(ExcelFileToRead);
        int numRow = wb.getSheetAt(0).getLastRowNum();
        List<Integer> listPatternNum = new ArrayList<>();
        for(int i = 1; i<=numRow; i++ ) {
            String str = String.valueOf(wb.getSheetAt(0).getRow(i).getCell(1));
            int num = Integer.parseInt(str);
            listPatternNum.add(num);
        }
        System.out.println(listPatternNum);
        Collections.sort(listPatternNum);
        System.out.println(listPatternNum);
        //String str = wb.getSheetAt(0).getRow(1).toString();
        //System.out.println(str);

    }



    public static void main(String[] args) throws IOException { 
        
       // csvToExcel("20Sept30org.csv");
      //  readExcel("test.xls");
    }
}